package com.company;

import java.security.Provider;
import java.security.Security;

import static java.security.Security.addProvider;

public class Main {

    public static void main(String[] args) throws Exception {

        addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	Start.start();
    }
}
