package com.company;

import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;

import static javax.print.attribute.standard.MediaSizeName.C;

public class Start {
    public static void start() throws GeneralSecurityException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");
        generator.initialize(2048);
        KeyPair keyPair = generator.generateKeyPair();

        PrivateKey privKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        // fetch the integers in the public key
        RSAPublicKey rsaPublicKey = (RSAPublicKey) publicKey;
        RSAPrivateKey rsaprivateKey = (RSAPrivateKey) privKey;
        BigInteger modulus = rsaPublicKey.getModulus();
        BigInteger moduluspriv = rsaprivateKey.getModulus();
        // print
        System.out.println(modulus.toString());
        System.out.println(moduluspriv.toString());
        // store (detail-dependant)
        byte[] modulusBytes = modulus.toByteArray();
        byte[] modulusBytesPriv = moduluspriv.toByteArray();
        String dir = "C:\\temp";
        String outFile = dir + "\\" + "modolus" +".txt";
        String outFilePriv = dir + "\\" + "modoluspriv" +".txt";
        FileUtils.write(outFile, modulusBytes);
        FileUtils.write(outFilePriv, modulusBytesPriv);

        // now I can access the exponent
        BigInteger publicExponent = rsaPublicKey.getPublicExponent();
        BigInteger privateExponent = rsaprivateKey.getPrivateExponent();
        System.out.println(publicExponent);
        System.out.println(privateExponent);

        //Read from file
        byte[] readExponentBytes = FileUtils.readAllBytes(outFile);
        byte[] readModulusBytes = FileUtils.readAllBytes(outFile);
        BigInteger readExponent = new BigInteger(readExponentBytes);
        BigInteger readModulus = new BigInteger(readModulusBytes);
        RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(readModulus, readExponent);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = fact.generatePrivate(keySpec);
        System.out.println(privateKey);

        //System.out.println(privKey);
        //System.out.println(publicKey);
    }
}

