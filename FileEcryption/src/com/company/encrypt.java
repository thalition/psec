package com.company;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.util.encoders.Hex;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.Scanner;

import static java.security.Security.addProvider;

public class encrypt {

    public static void startEncrypt() {
        System.out.println("starting encryption");
        Scanner in = new Scanner(System.in);
        System.out.println("password:");
        String inputKey = in.nextLine();
        while (inputKey.length()%16 != 0) {
            inputKey =inputKey+"00";
        }

        String resultKey = Methods.stringBuffer(inputKey);
        addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        String randomString = RandomString.getAlphaNumericString(32);
        String dir = "C:\\temp\\Encrypt";
        byte[] keyBytes = Hex.decode(resultKey);
        byte[] iv = Hex.decode(randomString);
        try {
            // reading medical record
            System.out.println("Encrypting medical record..");
            String inFile = dir + "\\" + "MedicalRecordNielsJ.pdf";
            byte [] input = FileUtils.readAllBytes(inFile);
            // encrypting
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
            SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");;
            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
            byte[] output = cipher.doFinal(input);
            // writing
            String outFile = dir + "\\" + "MedicalRecordNielsJ" + "." + randomString + "." + "aes";
            FileUtils.write(outFile, output);
            System.out.println("file encrypted: " + outFile);
            //getting digest
            System.out.println("getting digest for: "+input +"in file "+inFile);
            MessageDigest mDigest = MessageDigest.getInstance("AES", "BC");
            mDigest.update(input);
            byte[] hashValue = mDigest.digest();
            System.out.println(hashValue);
        } catch (Exception e) {}
    }

}
