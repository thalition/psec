package com.company;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.util.encoders.Hex;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.Scanner;

import static java.security.Security.addProvider;

public class decrypt {

    public static void startDecrypt() {
        System.out.println("starting decryption");
        Scanner in = new Scanner(System.in);
        System.out.println("password:");
        String inputKey = in.nextLine();
        while (inputKey.length()%16 != 0) {
            inputKey =inputKey+"00";
        }
        String resultKey = Methods.stringBuffer(inputKey);
        addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        String dir = "C:\\temp\\Encrypt";

        byte[] keyBytes = Hex.decode(resultKey);
        try {
            // finding the decrypted medical record
            String[] fileNames = FileUtils.getAllFileNames(dir, "sha-256"); // find all files with “.aes”
            String fileName = fileNames[0];
            String[] parts = fileName.split("[.]");
            // reading
            String inFile = dir + "\\" + fileName;
            byte[] input = FileUtils.readAllBytes(inFile);
            // decrypting
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
            SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
            String ivString = parts[1];
            //String ivString = getIV(fileName); // read the IV
            byte[] iv = Hex.decode(ivString);
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
            byte[] output = cipher.doFinal(input);
            // writing
            String outFile = dir + "\\" + parts[0] + "."  + "decrypted" + "." + "pdf";
            FileUtils.write(outFile, output);
            System.out.println("File Decrypted");
            //verify digest
            Methods.verify();
        } catch (Exception e) {}
    }
}
