package com.company;

import java.security.MessageDigest;

public class Methods {
    public static String stringBuffer(String n) {
        StringBuffer sb = new StringBuffer();
        char ch[] = n.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            String hexString = Integer.toHexString(ch[i]);
            sb.append(hexString);
        }
        return sb.toString();
    }
    public static void verify() {
        try {
            // reading medical record + stored hash
            System.out.println("Verifying hash of medical record..");
            String dir = "C:\\temp\\Encrypt";
            String inFile = dir + "\\" + "MedicalRecordNielsJ.pdf";
            String tamperedInFile = dir + "\\" + "MedicalRecordNielsJ.decrypted.pdf";
            byte[] storedHashValue = FileUtils.readAllBytes(inFile + ".sha256");

            // computing new hash
            MessageDigest mDigest = MessageDigest.getInstance("SHA-256", "BC");
            byte[] computedHashValue = FileUtils.readAllBytes(tamperedInFile);;
            // verifying
            if (computedHashValue == storedHashValue){
                System.out.println("Hash values are equal");
            } else{
                System.out.println("Hash values are not equal");
            }
        } catch (Exception e) {
        }
    }
}
