package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        start();

    }

    private static void start() {
        System.out.println("starting program");
        System.out.println("select program: 1:encrypt ");
        System.out.println("select program: 2:decrypt ");
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        int i = Integer.parseInt(s);
        if(i==1){encrypt.startEncrypt();}
        if(i==2){decrypt.startDecrypt();}
        else
        {
        start();
        }
    }
}
