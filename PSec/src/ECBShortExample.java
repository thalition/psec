import org.bouncycastle.util.encoders.Hex;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Scanner;

import static java.security.Security.addProvider;

public class ECBShortExample {
    public static void main(String[] args) throws Exception {
        addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()); // see p13
        /*Provider[] installedProvs = Security.getProviders();
        for (int i = 0; i != installedProvs.length; i++) {
            System.out.print(installedProvs[i].getName());
            System.out.print(": ");
            System.out.print(installedProvs[i].getInfo());
            System.out.println();

        }*/
        encrypt();
    }

    private static void encrypt() throws Exception  {
        Scanner in = new Scanner(System.in);
        String salt = "000102030405060708090a0b0c0d0e0a";
        System.out.println("Write something you want to encrypt");
        byte[] keyBytes = Hex.decode(salt);
        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding", "BC");
        String s = in.nextLine();
        //System.out.println(s);
        while (s.length()%16 != 0) {
            s = s+" ";
        }
        System.out.println("from sender: "+s);

        StringBuffer sb = new StringBuffer();
        char ch[] = s.toCharArray();
        for(int i = 0; i < ch.length; i++) {
            String hexString = Integer.toHexString(ch[i]);
            sb.append(hexString);
        }
        String result = sb.toString();

        byte[] input = Hex.decode(result);
        String encryptInput =Hex.toHexString(input);
        System.out.println("input: "+encryptInput);
        cipher.init(Cipher.ENCRYPT_MODE,key);
        byte[] output = cipher.doFinal(input);
        String encrypt =Hex.toHexString(output);
        System.out.println("encrypted: "+encrypt);
        cipher.init(Cipher.DECRYPT_MODE,key);
        String decrypt = Hex.toHexString(cipher.doFinal(output));
        System.out.println("decrypted: "+decrypt);

        String HextoString = new String();
        char[] charArray = result.toCharArray();
        for(int i = 0; i < charArray.length; i=i+2) {
            String st = ""+charArray[i]+""+charArray[i+1];
            char hs = (char)Integer.parseInt(st, 16);
            HextoString = HextoString + hs;
        }
        System.out.println("to receiver: "+HextoString);

        encrypt();
    }
}

